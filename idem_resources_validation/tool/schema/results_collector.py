import json

import yaml
from yaml import SafeDumper


class ResultsCollector:
    @property
    def findings(self):
        return self._findings

    @property
    def base_revision(self):
        return self._base_revision

    @property
    def current_revision(self):
        return self._current_revision

    @property
    def has_breaking_changes(self):
        return self._has_breaking_changes

    def __init__(self):
        self._findings = []
        self._base_revision = None
        self._current_revision = None
        self._has_breaking_changes = False

    def revisions(self, current_revision, base_revision):
        self._current_revision = current_revision
        self._base_revision = base_revision
        return self

    def add_breaking(self, schema_path, description):
        self._has_breaking_changes = True
        self._findings.append(
            {
                "schema_path": schema_path,
                "description": description,
                "is_breaking": True,
            }
        )

    def add_non_breaking(self, schema_path, description):
        self._findings.append(
            {
                "schema_path": schema_path,
                "description": description,
                "is_breaking": False,
            }
        )

    def report_as_json(self):
        report = {
            "base_revision": self.base_revision,
            "current_revision": self.current_revision,
            "findings": self.findings,
        }
        return json.dumps(report, indent=4)

    def report_as_yaml(self):
        report = {
            "base_revision": self.base_revision,
            "current_revision": self.current_revision,
            "findings": self.findings,
        }
        return yaml.dump(report, Dumper=SafeDumper)

    def report_only_breaking_changes_as_json(self):
        report = {
            "base_revision": self.base_revision,
            "current_revision": self.current_revision,
            "breaking_changes": list(
                filter(lambda x: x["is_breaking"] is True, self._findings)
            ),
        }
        return json.dumps(report, indent=4)
